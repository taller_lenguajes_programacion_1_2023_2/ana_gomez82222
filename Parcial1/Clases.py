class Restaurante:
    def __init__(self, idRestaurante, nombre, direccion, especialidad, horario, telefono, capacidad):
        self.idRestaurante = idRestaurante
        self.nombre = nombre
        self.direccion = direccion
        self.especialidad = especialidad
        self.horario=horario
        self.telefono=telefono
        self.capacidad=capacidad

    def __str__(self):
        return f'Restaurante(id={self.idRestaurante}, nombre={self.nombre}, direccion={self.direccion}, especialidad={self.especialidad}, horario={self.horario}, telefono={self.telefono}, capacidad{self.capacidad})'

class Plato:
    def __init__(self, idPlato, nombre, ingredientes, precio, tiempoPreparacion, tipo, alergenos):
        self.idPlato = idPlato
        self.nombre=nombre
        self.ingredientes=ingredientes
        self.precio=precio
        self.tiempoPreparacion=tiempoPreparacion
        self.tipo=tipo
        self.alergenos=alergenos

    def __str__(self):
        return f'Plato(id={self.idPlato}, nombre={self.nombre}, ingredientes={self.ingredientes}, precio={self.precio}, tiempoPreparacion={self.tiempoPreparacion}, tipo={self.tipo}, alergenos={self.alergenos})'

class Cliente:
    def __init__(self, idCliente, nombre, telefono, direccion, alergias, preferencias, correoElect):
        self.idCliente = idCliente
        self.nombre = nombre
        self.telefono=telefono
        self.direccion = direccion
        self.alergias=alergias
        self.preferencias=preferencias
        self.correoElect=correoElect

    def __str__(self):
        return f'Cliente(id={self.idCliente}, nombre={self.nombre}, telefono={self.telefono}, direccion={self.direccion}, alergias={self.alergias}, preferencias={self.preferencias}, correoElect={self.correoElect})'

class Orden(Restaurante, Cliente):
    def __init__(self, idOrden, numeroOrden, fechaOrden, cliente, restaurante, total):
        super().__init__(idOrden, restaurante, cliente)
        self.idOrden=idOrden
        self.numeroOrden=numeroOrden
        self.fechaOrden=fechaOrden
        self.total=total

    def __str__(self):
        return f'Orden(id={self.idOrden}, fechaOrden={self.fechaOrden}, idCliente={self.idCliente}, idRestaurante={self.idRestaurante}, total={self.total})'
    

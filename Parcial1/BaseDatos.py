import sqlite3

# Conecta a la base de datos SQLite
conexion = sqlite3.connect('BaseDatos.db')
cursor = conexion.cursor()

cursor.execute('''
    CREATE TABLE IF NOT EXISTS Restaurante (
        idRestaurante INTEGER PRIMARY KEY,
        nombre TEXT,
        direccion TEXT,
        especialidad TEXT, 
        horario TEXT,
        telefono TEXT,
        capacidad INT
    )
''')

cursor.execute('''
    CREATE TABLE IF NOT EXISTS Plato (
        idPlato INTEGER PRIMARY KEY,
        nombre TEXT,
        ingredientes TEXT, 
        precio INT, 
        tiempoPreparacion TEXT,
        tipo TEXT,
        alergenos TEXT
    )
''')

cursor.execute('''
    CREATE TABLE IF NOT EXISTS Cliente (
        idCliente INTEGER PRIMARY KEY,
        nombre TEXT,
        telefono TEXT,
        direccion TEXT,
        alergias TEXT,
        preferencias TEXT, 
        correoElect TEXT
    )
''')

cursor.execute('''
    CREATE TABLE IF NOT EXISTS Orden (
        idOrden INTEGER PRIMARY KEY,
        numeroOrden TEXT, 
        fechaOrden DATE, 
        cliente TEXT, 
        restaurante TEXT, 
        total INT,
        FOREIGN KEY (cliente) REFERENCES Cliente (idCliente),
        FOREIGN KEY restaurante REFERENCES Restaurante(idRestaurante)
    )
''')

# Cierra
conexion.close()

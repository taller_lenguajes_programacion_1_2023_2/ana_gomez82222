import sqlite3
import json

class CRUD:
    def __init__(self, BaseDatos):
        self.conexion = sqlite3.connect(BaseDatos)
        self.cursor = self.conexion.cursor()

    def crear_tabla(self, nombreTabla, estructura):
        self.cursor.execute(f'CREATE TABLE IF NOT EXISTS {nombreTabla} ({estructura})')

    def insertar_registro(self, nombreTabla, datos):
        columnas = ', '.join(datos.keys())
        valores = ', '.join(['?' for _ in datos])
        consulta = f'INSERT INTO {nombreTabla} ({columnas}) VALUES ({valores})'
        self.cursor.execute(consulta, list(datos.values()))
        self.conexion.commit()

    def seleccionar_registros(self, nombreTabla):
        self.cursor.execute(f'SELECT * FROM {nombreTabla}')
        return self.cursor.fetchall()

    def cerrar_conexion(self):
        self.conexion.close()

# Carga las consultas desde un archivo JSON
with open('consultas.json', 'r') as archivo_json:
    consultas = json.load(archivo_json)

import sqlite3
import json

class ConexionBaseDeDatos:
    def __init__(self, nombre_base_de_datos):
        self.conexion = sqlite3.connect(nombre_base_de_datos)
        self.cursor = self.conexion.cursor()

    def crear_tabla(self, nombre_tabla, estructura):
        self.cursor.execute(f'CREATE TABLE IF NOT EXISTS {nombre_tabla} ({estructura})')

    def insertar_registro(self, nombre_tabla, datos):
        columnas = ', '.join(datos.keys())
        valores = ', '.join(['?' for _ in datos])
        consulta = f'INSERT INTO {nombre_tabla} ({columnas}) VALUES ({valores})'
        self.cursor.execute(consulta, list(datos.values()))
        self.conexion.commit()

    def seleccionar_registros(self, nombre_tabla):
        self.cursor.execute(f'SELECT * FROM {nombre_tabla}')
        return self.cursor.fetchall()

    def cerrar_conexion(self):
        self.conexion.close()

# Carga las consultas desde un archivo JSON
with open('consultas.json', 'r') as archivo_json:
    consultas = json.load(archivo_json)

# Ejemplo de uso
conexion = ConexionBaseDeDatos('mi_base_de_datos.db')
conexion.crear_tabla('Clase1', 'id INTEGER PRIMARY KEY, atributo1 TEXT, atributo2 TEXT, atributo3 TEXT')
conexion.insertar_registro('Clase1', {'atributo1': 'valor1', 'atributo2': 'valor2', 'atributo3': 'valor3'})
registros = conexion.seleccionar_registros('Clase1')
print(registros)
conexion.cerrar_conexion()

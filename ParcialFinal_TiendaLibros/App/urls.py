"""
URL configuration for App project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Inicio.views import index, signIn, signUp,usuarios,eliminarUser
from Libros.views import nuevoLibro,libros,generoLibros,verLibro,eliminarLibro

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index),
    path('SignIn/', signIn),
    path('SignUp/', signUp),
    path('AdminUsers/', usuarios),
    path('AdminUser/<int:id>/', eliminarUser, name="eliminarUser"),
    path('AdminLibros/', nuevoLibro),
    path('AdminLibros/<int:id>/', eliminarLibro, name="eliminarLibro"),
    path('TiendaLibros/', libros),
    path('Libros_Genero/<str:genero>/', generoLibros, name="generoLibros"),
    path('Libro/<int:id>/', verLibro, name="Libro"),
    
]

from django.shortcuts import render, redirect
from . models import Usuarios

# Create your views here.
def index(request):
    return render(request, 'Index.html')

def signIn(request):
    error=""
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        
        try:
            userEncontrado = Usuarios.objects.get(username=username)
            if userEncontrado.password == password:
                return redirect('/TiendaLibros/')
            else:
                error = "Contraseña incorrecta"
        except:
            error="Usuario no encontrado"
            
    return render(request, 'SignIn.html',{
        "error" : error,
    })
def signUp(request):
    error=""
    if request.method == 'POST':
        nombre = request.POST['nombre']
        apellido = request.POST['apellido']
        username = request.POST['username']
        password = request.POST['password']
        
        try:
            usuarioExiste = Usuarios.objects.get(username=username)
            error="El usuario ya existe"
        except:
            nuevoUsuario = Usuarios.objects.create(nombre=nombre, apellido=apellido, username=username, password=password)
            return redirect('/SignIn/')
    return render(request, 'SignUp.html')
#Admin Usuarios
def usuarios(request):
    listaUsuarios=Usuarios.objects.all()
    return render(request, 'AdminUsers.html', { "users": listaUsuarios})
def eliminarUser(request, id):
    user=Usuarios.objects.get(id=id)
    user.delete()
    return redirect('/AdminUsers/')
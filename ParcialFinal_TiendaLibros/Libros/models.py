from django.db import models

# Create your models here.
class Libros(models.Model):
    id=models.AutoField(primary_key=True)
    titulo=models.CharField(max_length=70)
    autor=models.CharField(max_length=60)
    genero=models.CharField(max_length=50)
    precio=models.IntegerField()
    portada=models.CharField(max_length=150)

from django.shortcuts import render, redirect
from django.db import connection
from .models import Libros

# Create your views here.
def nuevoLibro(request):
    mensaje=""
    if request.method == 'POST':
        titulo = request.POST['titulo']
        autor = request.POST['autor']
        genero = request.POST['genero']
        precio = request.POST['precio']
        portada = request.POST['portada']
        try:
            LibrosExiste = Libros.objects.get(titulo=titulo)
            error="Este libro ya existe"
        except:
            nuevo = Libros.objects.create(titulo=titulo, autor=autor, genero=genero, precio=precio, portada=portada )
            mensaje="El libro se agrego correctamente"
            return redirect('/AdminLibros/')
    TodosLibros=Libros.objects.all()
    return render(request, 'AdminLibros.html', {"libros":TodosLibros})

def eliminarLibro(request, id):
    user=Libros.objects.get(id=id)
    user.delete()
    return redirect('/AdminLibros/')

def libros(request):
    listaTodosLibros=Libros.objects.all()
    idLibro=id
    return render(request, 'TiendaLibros.html', {"libros": listaTodosLibros, "id": idLibro})

def generoLibros(request, genero):
    listaLibros=Libros.objects.filter(genero=genero).all()
    gen=genero
    return render(request, 'Libros_Genero.html', { "libros_genero": listaLibros,"genero": gen})

def verLibro(request, id):
    libro=Libros.objects.filter(id=id).all()
    return render(request, 'Libro.html', { "libro": libro})
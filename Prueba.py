import tkinter as tk
from tkinter import ttk
import sqlite3
import json

class CRUDApp:
    def __init__(self, root):
        self.root = root
        self.root.title("CRUD App")
        
        self.connection = sqlite3.connect("library.db")
        self.cursor = self.connection.cursor()
        
        self.queries = {
            "CrearTabla": "CREATE TABLE if not exists {} ( {} )",
            "InsertarDatos": "INSERT INTO {} VALUES (NULL, {} )",
            "SeleccionarTodo": "SELECT * FROM {}",
            "ActualizarDatos": "UPDATE {} SET {}='{}' WHERE {}={}",
            "EliminarDatos": "DELETE FROM {} WHERE {}={}",
            "Library": "idLibrary INTEGER primary key, libraryName VARCHAR(45) not null, city VARCHAR(30) not null",
            "Books": "idBook INTEGER primary key, bookTitle VARCHAR(45) not null, authorName VARCHAR(45) not null, libraryId INT NOT NULL, FOREIGN KEY (libraryId) REFERENCES Libraries (idLibrary)"
        }
        
        with open("queries.json", "w") as query_file:
            json.dump(self.queries, query_file, indent=4)
        
        self.create_table("Libraries", self.queries["Library"])
        self.create_table("Books", self.queries["Books"])
        
        self.create_ui()
    
    def create_table(self, table_name, columns):
        create_query = self.queries["CrearTabla"].format(table_name, columns)
        self.cursor.execute(create_query)
        self.connection.commit()
    
    def create_ui(self):
        self.treeview = ttk.Treeview(self.root, columns=("ID", "Library Name", "City"))
        self.treeview.heading("#1", text="ID")
        self.treeview.heading("#2", text="Library Name")
        self.treeview.heading("#3", text="City")
        
        self.treeview.grid(row=0, column=0, padx=10, pady=10, columnspan=2)
        
        self.load_data()
        
        self.treeview.bind("<ButtonRelease-1>", self.select_item)
        
        self.label_library_name = tk.Label(self.root, text="Library Name:")
        self.label_library_name.grid(row=1, column=0, padx=10, pady=10, sticky="e")
        
        self.label_city = tk.Label(self.root, text="City:")
        self.label_city.grid(row=2, column=0, padx=10, pady=10, sticky="e")
        
        self.entry_library_name = tk.Entry(self.root)
        self.entry_library_name.grid(row=1, column=1, padx=10, pady=10)
        
        self.entry_city = tk.Entry(self.root)
        self.entry_city.grid(row=2, column=1, padx=10, pady=10)
        
        self.button_add = tk.Button(self.root, text="Add", command=self.add_library)
        self.button_add.grid(row=3, column=0, padx=10, pady=10, sticky="w")
        
        self.button_update = tk.Button(self.root, text="Update", command=self.update_library)
        self.button_update.grid(row=3, column=1, padx=10, pady=10, sticky="w")
        
        self.button_delete = tk.Button(self.root, text="Delete", command=self.delete_library)
        self.button_delete.grid(row=3, column=1, padx=10, pady=10, sticky="e")
        
    def load_data(self):
        select_query = self.queries["SeleccionarTodo"].format("Libraries")
        self.cursor.execute(select_query)
        rows = self.cursor.fetchall()
        
        for row in rows:
            self.treeview.insert("", "end", values=row)
    
    def add_library(self):
        library_name = self.entry_library_name.get()
        city = self.entry_city.get()
        
        insert_query = self.queries["InsertarDatos"].format("Libraries", f"'{library_name}', '{city}'")
        self.cursor.execute(insert_query)
        self.connection.commit()
        
        self.entry_library_name.delete(0, "end")
        self.entry_city.delete(0, "end")
        self.load_data()
    
    def update_library(self):
        selected_item = self.treeview.selection()
        
        if not selected_item:
            return
        
        library_name = self.entry_library_name.get()
        city = self.entry_city.get()
        
        item_id = self.treeview.item(selected_item, "values")[0]
        
        update_query = self.queries["ActualizarDatos"].format("Libraries", "libraryName", library_name, "idLibrary", item_id)
        self.cursor.execute(update_query)
        
        update_query = self.queries["ActualizarDatos"].format("Libraries", "city", city, "idLibrary", item_id)
        self.cursor.execute(update_query)
        
        self.connection.commit()
        
        self.entry_library_name.delete(0, "end")
        self.entry_city.delete(0, "end")
        self.load_data()
    
    def delete_library(self):
        selected_item = self.treeview.selection()
        
        if not selected_item:
            return
        
        item_id = self.treeview.item(selected_item, "values")[0]
        
        delete_query = self.queries["EliminarDatos"].format("Libraries", "idLibrary", item_id)
        self.cursor.execute(delete_query)
        self.connection.commit()
        
        self.load_data()
    
    def select_item(self, event):
        selected_item = self.treeview.selection()
        if selected_item:
            values = self.treeview.item(selected_item, "values")
            self.entry_library_name.delete(0, "end")
            self.entry_city.delete(0, "end")
            self.entry_library_name.insert(0, values[1])
            self.entry_city.insert(0, values[2])

if __name__ == "__main__":
    root = tk.Tk()
    app = CRUDApp(root)
    root.mainloop()

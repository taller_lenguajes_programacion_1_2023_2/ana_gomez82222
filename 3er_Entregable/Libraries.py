from conexion import conexion

class Libraries(conexion):
    def __init__(self, libraryName="", city=""):
        """
        Inicializa una instancia de la clase.

        Parámetros:
            libraryName (str): El nombre de la biblioteca (por defecto es una cadena vacía).
            city (str): El nombre de la ciudad (por defecto es una cadena vacía).

        Retorna:
            Ninguno
        """
        conexion.__init__(self)
        self.libraryName=libraryName
        self.city=city
        self.crear("Librerias","Library")
        
    def agregar(self):
        """
        Esta función se utiliza para agregar datos a la tabla "Librerias" en la base de datos.

        Parámetros:
            Ninguno

        Retorna:
            Ninguno
        """
        datos = [
            (f"{self.libraryName}",f"{self.city}")
        ]
        self.insertar("Librerias",2,datos)
        
    def verTabla(self):
        """
        Recupera los datos de la tabla "Librerias" y los asigna a la variable de instancia "datosEnLaTabla".

        Parámetros:
            self (objeto): La instancia de la clase.

        Retorna:
            Ninguno
        """
        self.datosEnLaTabla = self.seleccionarTabla("Librerias")

from conexion import conexion

class Books(conexion):
    def __init__(self, bookTitle="", authorName="", libraryId=""):
        conexion.__init__(self)
        self.bookTitle=bookTitle
        self.authorName=authorName
        self.libraryId=libraryId
        self.crear("Libros","Books")
        
            
    def agregar(self):
        """Toma los datos y los pone en la base de datos en el orden que se aprecia
        """
        datos = [
            (f"{self.bookTitle}",f"{self.authorName}",f"{self.libraryId}")
        ]
        self.insertar("Libros",3,datos)
            
    def verTabla(self): 
        """Permite llamar con la función a la tabla para verla
        """
        self.datosEnTabla = self.seleccionarTabla("Libros")

import sqlite3
import json

class conexion:
    def __init__(self):
        self.baseDatos=sqlite3.connect("3er_Entregable/DB.sqlite3")
        self.pointer=self.baseDatos.cursor()
        with open("3er_Entregable/queries.json")as queries:
            self.query=json.load(queries)

    def crear(self,nombre,columnas):
        """
        Crea una nueva tabla en la base de datos.

        Argumentos:
            nombre (str): El nombre de la tabla.
            columnas (str): Las columnas de la tabla.

        Retorna:
            None
        """
        queryCrear=self.query["CrearTabla"].format(nombre, self.query[columnas])
        self.pointer.execute(queryCrear)
        self.baseDatos.commit()


    def insertar(self, tabla, columnas, datos):
        """
        Inserta datos en una tabla especificada.

        Parámetros:
            tabla (str): El nombre de la tabla en la que se insertarán los datos.
            columnas (int): El número de columnas en la tabla.
            datos (list): Una lista de datos para insertar en la tabla.

        Retorna:
            None
        """
        queryInsertar = self.query["InsertarDatos"].format(tabla, ', '.join(['?'] * columnas))
        self.pointer.executemany(queryInsertar,datos)
        self.baseDatos.commit()
        
    def seleccionarTabla(self, tabla):
        """
        Ejecuta una consulta para seleccionar todos los datos de la tabla especificada.

        Parámetros:
            tabla (str): El nombre de la tabla de la cual se seleccionarán los datos.

        Retorna:
            list: Una lista que contiene los datos seleccionados.
        """
        querySeleccionarTabla = self.query["SeleccionarTodo"].format(tabla)
        self.pointer.execute(querySeleccionarTabla)
        contenido = self.pointer.fetchall()
        return contenido
    
    def actualizar(self, tabla, columna, nuevovalor, id):
        """
        Actualiza una columna especificada en una tabla especificada con un nuevo valor basado en el ID dado.

        Args:
            tabla (str): El nombre de la tabla a actualizar.
            columna (str): El nombre de la columna a actualizar.
            nuevovalor (str): El nuevo valor a establecer en la columna especificada.
            id (int): El ID para identificar la fila a actualizar.

        Returns:
            None
        """
        queryActualizar = self.query["ActualizarDatos"].format(tabla,columna,nuevovalor,"ID",id)
        self.pointer.execute(queryActualizar)
        self.baseDatos.commit()
        
    def eliminar(self, tabla, id):
        """
        Elimina un registro de la tabla especificada basado en el ID dado.

        Parámetros:
            tabla (str): El nombre de la tabla de la cual se eliminará el registro.
            id (int): El ID del registro a eliminar.

        Retorna:
            None
        """
        queryEliminar = self.query["EliminarDatos"].format(tabla,"ID",id)
        self.pointer.execute(queryEliminar)
        self.baseDatos.commit()
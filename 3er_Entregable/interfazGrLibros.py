import tkinter as tk
from tkinter import messagebox,ttk
from Books import Books

class interfazGrLibros:
    def __init__(self):
        self.winPrincipal = tk.Tk()
        self.winPrincipal.title("Libros")
        self.frameDatosBooks = tk.Frame(self.winPrincipal)
        self.frameDatosBooks.grid(row=0, column=0, padx=5, pady=5)
        self.frameBotonesP= tk.Frame(self.winPrincipal)
        self.frameBotonesP.grid(row=1, column=0, padx=5, pady=5)
        self.frameBaseDeDatos = tk.Frame(self.winPrincipal)
        self.frameBaseDeDatos.grid(row=2, column=0, padx=5, pady=5)
        self.frameBotonesLat = tk.Frame(self.winPrincipal)
        self.frameBotonesLat.grid(row=2,column=1, padx=5, pady=5)
        
        self.Books = Books()
        self.campos()
        self.botonesPrincipales()
        self.crear()
        self.vincularBaseDeDatos()
        self.botonesLat()
        self.winPrincipal.mainloop()
        
    #Se crea la función para crear tabla
    def crear(self):  
        """
        Inicializa y configura un widget Treeview para la tabla base de la base de datos.
        Este método crea un nuevo widget Treeview y lo configura para mostrar las columnas
        "idBook", "bookTitle", "authorName" y "libraryId". También establece los encabezados para
        cada columna. Por último, coloca el widget Treeview en la ubicación especificada en la rejilla
        cuadrícula.
        Parámetros:
            Ninguno
        Devuelve:
            Ninguno
        """
        self.tablaBaseDeDatos = ttk.Treeview(self.frameBaseDeDatos,show="headings")
        self.tablaBaseDeDatos.config(columns=("ID", "bookTitle", "authorName", "libraryId"))
        self.tablaBaseDeDatos.heading("ID", text="ID")
        self.tablaBaseDeDatos.heading("bookTitle", text="bookTitle")
        self.tablaBaseDeDatos.heading("authorName", text="authorName")
        self.tablaBaseDeDatos.heading("libraryId", text="libraryId")
        self.tablaBaseDeDatos.grid(row=0, column=0)
    
    #Se crea una función para definir los campos que se muestran en la interfaz
    def campos(self):
        """
        Inicializa y configura la función campos.
        Parámetros:
            self: La instancia de la clase.
        Devuelve:
            Vacío
        """
        self.varTit=tk.StringVar()
        self.txtTit=tk.Label(self.frameDatosBooks,text="LIBROS")
        self.txtTit.config(font=("arial black",16), foreground = "#1379ED")#Define la fuente del texto, su tamaño y su color
        self.txtTit.grid(row=0, column=0, padx=3, pady=3)
        
        self.varTitulo = tk.StringVar()
        self.txtTitulo = tk.Label(self.frameDatosBooks, text="Titulo: ")
        self.txtTitulo.grid(row=1, column=0)
        self.txtTitulo.config(font=("arial",12))
        self.cuadroTitulo = tk.Entry(self.frameDatosBooks, textvariable=self.varTitulo)
        self.cuadroTitulo.grid(row=1, column=1)
        
        self.varAutor = tk.StringVar()
        self.txtAutor = tk.Label(self.frameDatosBooks, text="Autor: ")
        self.txtAutor.grid(row=2, column=0)
        self.txtAutor.config(font=("arial",12))
        self.cuadroAutor = tk.Entry(self.frameDatosBooks, textvariable=self.varAutor)
        self.cuadroAutor.grid(row=2, column=1)
        
        self.varLibreria = tk.StringVar()
        self.txtLibreria = tk.Label(self.frameDatosBooks, text="Libreria: ")
        self.txtLibreria.grid(row=3, column=0)
        self.txtLibreria.config(font=("arial",12))#Define la fuente del texto y su tamaño
        self.cuadroLibreria = tk.Entry(self.frameDatosBooks, textvariable=self.varLibreria)
        self.cuadroLibreria.grid(row=3, column=1)

    #Se crean los botones principales
    def botonesPrincipales(self):
        """
        Incializa y configura los botones principales.
        """
        self.botonNuevo = tk.Button(self.frameBotonesP,text="Nuevo", bg="#1379ED",font=("arial",11), command=self.funcionNuevo)
        self.botonNuevo.grid(row=0, column=0, padx=5)#Define la posición del botón
        
        self.botonGuardar = tk.Button(self.frameBotonesP,text="Guardar", bg="#1379ED",font=("arial",11), command=self.funcionGuardar)
        self.botonGuardar.grid(row=0, column=1, padx=5)#Define la posición del botón
        
        self.botonCancelar = tk.Button(self.frameBotonesP,text="Cancelar", bg="#1379ED",font=("arial",11), command=self.funcionCancelar)
        self.botonCancelar.grid(row=0, column=2, padx=5)#Define la posición del botón
    
    #Se define la función para el boton "Nuevo"
    def funcionNuevo(self):
        """
        Restablece los valores de las variables y configura el estado de los elementos de la GUI.
        """
        self.varTitulo.set('')
        self.varAutor.set('')
        self.varLibreria.set('')       
        self.cuadroTitulo.config(state='normal')
        self.cuadroAutor.config(state='normal')
        self.cuadroLibreria.config(state='normal')     
        self.botonGuardar.config(state='normal')
        self.botonCancelar.config(state='normal')
        
    #Se define la función para el boton "Cancelar"
    def funcionCancelar(self):
        """
        Restablece los valores de las variables titulo, autor y libreria a vacío y deshabilita
        los elementos de la GUI.
        Desactiva los botones de guardar y cancelar.
        """
        self.varTitulo.set('')
        self.varAutor.set('')
        self.varLibreria.set('')       
        self.cuadroTitulo.config(state='disabled')
        self.cuadroAutor.config(state='disabled')
        self.cuadroLibreria.config(state='disabled')     
        self.botonGuardar.config(state='disabled')
        self.botonCancelar.config(state='disabled')        
    
    #Se define la función para el boton "Guardar"
    def funcionGuardar(self):
        """
        Guarda los detalles del libro en la base de datos.

        Esta función recupera el título del libro, el nombre del autor y el ID de la biblioteca de los respectivos campos de entrada
        en la GUI. A continuación, asigna estos valores a los atributos correspondientes del objeto Books. 
        La función `agregar()` se ejecuta en el objeto Books para añadir el libro a la base de datos. 
        Por último, se llama a la función `vincularBaseDeDatos()` para actualizar la base de datos.

        Parámetros:
            self (objeto): La instancia de la clase actual.

        Devuelve:
            Ninguno
        """
        self.Books.bookTitle = self.varTitulo.get()
        self.Books.authorName = self.varAutor.get()
        self.Books.libraryId = self.varLibreria.get()
        self.Books.agregar()
        self.vincularBaseDeDatos()
    
    #Funcion para vincular la base de datos
    def vincularBaseDeDatos(self):
        """
        Vincula la base de datos con la tabla en la interfaz gráfica.

        Parameters:
            self: La instancia actual de la clase.
        
        Returns:
            None
        """
        self.Books.verTabla()
        self.tablaBaseDeDatos.delete(*self.tablaBaseDeDatos.get_children())
        for fila in self.Books.datosEnTabla:
            self.tablaBaseDeDatos.insert("","end",values=fila)
    
    #Se crean los botones laterales
    def botonesLat(self):
        """
        Inicializa y configura los botones para el marco lateral.

        Esta función crea y configura los botones utilizados en el marco lateral de la interfaz gráfica.
        Configura la etiqueta y el campo de entrada para el ID, el botón "Editar" y el botón "Eliminar".
        La etiqueta y el campo de entrada del ID se utilizan para ingresar y mostrar un valor de ID.
        El botón "Editar" llama al método funcionEditar() cuando se hace clic en él.
        El botón "Eliminar" llama al método funcionEliminar() cuando se hace clic en él.

        Parámetros:
            Ninguno

        Retorna:
            Ninguno
        """  
        self.variableID = tk.IntVar()
        self.textoID = tk.Label(self.frameBotonesLat, text="ID: ")
        self.textoID.grid(row=0, column=0)
        self.cuadroID = tk.Entry(self.frameBotonesLat, textvariable=self.variableID)
        self.cuadroID.grid(row=1, column=0)        
        
        self.botonEditar = tk.Button(self.frameBotonesLat,text="Editar", bg="#1379ED", command=self.funcionEditar)
        self.botonEditar.grid(row=2, column=0, pady=5)
        
        self.botonEliminar = tk.Button(self.frameBotonesLat,text="Eliminar", bg="#1379ED", command=self.funcionEliminar)
        self.botonEliminar.grid(row=3, column=0, pady=5)  
    
    #Se define la función para el boton "Editar"
    def funcionEditar(self):
        """
        Actualiza la información del libro en la base de datos en función de los parámetros proporcionados.
        
        Parámetros:
            Ninguno
            
        Retorna:
            Ninguno
        """
        if self.varTitulo.get():
            self.Books.actualizar("Libros","bookTitle",self.varTitulo.get(),self.variableID.get())   
            
        if self.varAutor.get():
            self.Books.actualizar("Libros","authorName",self.varAutor.get(),self.variableID.get())  
            
        if self.varLibreria.get():
            self.Books.actualizar("Libros","LibraryId",self.varLibreria.get(),self.variableID.get())  
        
        self.vincularBaseDeDatos()
    
    #Se define la función para el boton "Eliminar"
    def funcionEliminar(self):
        """
        Esta función se encarga de eliminar datos de la tabla "Books" en la base de datos.
        No toma parámetros y no devuelve nada.

        Esta función llama al método "eliminar" del objeto "Books" para eliminar los datos.
        Luego muestra un mensaje informativo utilizando la función "messagebox.showinfo".
        Después de eso, llama al método "vincularBaseDeDatos" para actualizar la vista de la base de datos.
        """
        self.Books.eliminar("Libros",self.variableID.get())
        messagebox.showinfo("Base de datos Books","Se eliminaron los datos en la tabla.")
        self.vincularBaseDeDatos()
        
aplicacion = interfazGrLibros()
import tkinter as tk
from tkinter import messagebox,ttk
from Libraries import Libraries

class interfazGrLibrerias:
    def __init__(self):
        self.winPrincipal = tk.Tk()
        self.winPrincipal.title("Librerias")#Título de la interfaz
        self.frameDatosLibrary= tk.Frame(self.winPrincipal)
        self.frameDatosLibrary.grid(row=0,column=0)
        self.frameBotonesP=tk.Frame(self.winPrincipal)
        self.frameBotonesP.grid(row=1,column=0, padx=4,pady=4)
        self.frameBaseDatos = tk.Frame(self.winPrincipal)
        self.frameBaseDatos.grid(row=2, column=0, padx=5, pady=5)
        self.frameBotonesLat = tk.Frame(self.winPrincipal)
        self.frameBotonesLat.grid(row=2,column=1, padx=5, pady=5)
        
        self.Libraries=Libraries()
        self.botonesPrincipales()#Mostrar en la interfaz los botones principales
        self.campos()#Mostrar en la interfaz los campos
        self.crearTabla()#Mostrar en la interfaz la tabla
        self.vincularBaseDatos()#Mostrar en la interfaz los datos de la base de datos
        self.botonesLat()#Mostrar en la interfaz los botones laterales
        self.winPrincipal.mainloop()
        
    #Se crea la función para crear tabla
    def crearTabla(self):
        """
        Crea una tabla en la interfaz de usuario para mostrar los datos de la base de datos.
        Parámetros:
            Ninguno
            
        Devuelve:
            Ninguno
        """
        self.tablaBaseDatos = ttk.Treeview(self.frameBaseDatos,show="headings")
        self.tablaBaseDatos.config(columns=("ID", "libraryName", "city"))
        self.tablaBaseDatos.heading("ID", text="ID")
        self.tablaBaseDatos.heading("libraryName", text="NOMBRE")
        self.tablaBaseDatos.heading("city", text="CIUDAD")
        self.tablaBaseDatos.grid(row=0, column=0)
        
    #Se crea una función para definir los campos que se muestran en la interfaz
    def campos(self):
        """
        Inicializa y configura la función campos.
        Parámetros:
            self: La instancia de la clase.
        Devuelve:
            Vacío
        """
        self.varTitle=tk.StringVar()
        self.txtTitle=tk.Label(self.frameDatosLibrary,text="LIBRERIAS")#Es el título principal
        self.txtTitle.config(font=("arial black",16), foreground = "#1379ED")#Define la fuente del texto, su tamaño y su color
        self.txtTitle.grid(row=0, column=0, padx=3, pady=3)
        
        self.varNombre=tk.StringVar()
        self.txtNombre=tk.Label(self.frameDatosLibrary,text="Nombre: ")
        self.txtNombre.grid(row=1, column=0)
        self.txtNombre.config(font=("arial",12))
        self.cuadroNombre=tk.Entry(self.frameDatosLibrary, textvariable=self.varNombre)
        self.cuadroNombre.grid(row=1, column=1, padx=3, pady=3)
        
        self.varCiudad=tk.StringVar()
        self.txtCiudad=tk.Label(self.frameDatosLibrary,text="Ciudad: ")
        self.txtCiudad.grid(row=2, column=0)
        self.txtCiudad.config(font=("arial",12))
        self.cuadroCiudad=tk.Entry(self.frameDatosLibrary, textvariable=self.varCiudad)
        self.cuadroCiudad.grid(row=2, column=1, padx=3, pady=3)
        
    #Funcion para crear los botones principales
    def botonesPrincipales(self):
        """
        Genera los botones principales de la interfaz.
        Esta función crea y configura tres botones: "Nuevo", "Guardar" y "Cancelar".
        Cada botón está asociado a un comando específico.
        """
        self.botonNuevo=tk.Button(self.frameBotonesP,text="Nuevo",bg="#1379ED",font=("arial",11),command=self.funcionNuevo)
        self.botonNuevo.grid(row=0,column=0,padx=3)#Define la posición del botón
        
        self.botonGuardar=tk.Button(self.frameBotonesP,text="Guardar",bg="#1379ED",font=("arial",11),command=self.funcionGuardar)
        self.botonGuardar.grid(row=0,column=1,padx=3)#Define la posición del botón
        
        self.botonCancelar=tk.Button(self.frameBotonesP,text="Cancelar",bg="#1379ED",font=("arial",11),command=self.funcionCancelar)
        self.botonCancelar.grid(row=0,column=2,padx=3)#Define la posición del botón
    
    #Se define la función para el boton "Nuevo"
    def funcionNuevo(self):
        """
        Restablece los valores de las variables de nombre y ciudad a una cadena vacía.
        Establece el estado de los campos de entrada de nombre y ciudad a 'normal'.
        Establece el estado de los botones de guardar y cancelar a 'normal'.
        """
        self.varNombre.set('')
        self.varCiudad.set('')
        self.cuadroNombre.config(state='normal')
        self.cuadroCiudad.config(state='normal')   
        self.botonGuardar.config(state='normal')
        self.botonCancelar.config(state='normal')
        
    #Se define laSetBranchion para el boton "Cancelar"
    def funcionCancelar(self):
        """
        Reinicia los valores de las variables nombre y ciudad a una cadena vacía.
        Desactiva las cajas de texto cuadroNombre y cuadroCiudad.
        Desactiva los botones botonGuardar y botonCancelar.
        """
        self.varNombre.set('')
        self.varCiudad.set('')      
        self.cuadroNombre.config(state='disabled')
        self.cuadroCiudad.config(state='disabled') 
        self.botonGuardar.config(state='disabled')
        self.botonCancelar.config(state='disabled')        
        
    #Se define la función para el boton "Guardar"
    def funcionGuardar(self):
        """
        Guarda los datos de la biblioteca actual en la base de datos.

        Parameters:
            None

        Returns:
            None
        """
        self.Libraries.libraryName = self.varNombre.get()
        self.Libraries.city = self.varCiudad.get()
        self.Libraries.agregar()
        self.vincularBaseDatos()#Llamamos la fucion que vincula todo a la base de datos
    
    #Funcion para vincular la base de datos
    def vincularBaseDatos(self):
        """
        Esta función se encarga de vincular la base de datos.

        Argumentos:
        Ninguno

        Retorna:
        Ninguno
        """
        self.Libraries.verTabla()
        self.tablaBaseDatos.delete(*self.tablaBaseDatos.get_children())
        for fila in self.Libraries.datosEnLaTabla:
            self.tablaBaseDatos.insert("","end",values=fila)
    
    #Funcion para vincular los botones laterales
    def botonesLat(self):
        """
        Inicializa y configura los botones en el lateral de la interfaz gráfica.
        """
        self.varID = tk.IntVar()
        self.textoID = tk.Label(self.frameBotonesLat, text="ID: ")
        self.textoID.grid(row=0, column=0)
        self.cuadroID = tk.Entry(self.frameBotonesLat, textvariable=self.varID)
        self.cuadroID.grid(row=1, column=0)        
        
        self.botonEditar = tk.Button(self.frameBotonesLat,text="Editar", bg="#1379ED", command=self.funcionEditar)
        self.botonEditar.grid(row=2, column=0, pady=5)
        
        self.botonEliminar = tk.Button(self.frameBotonesLat,text="Eliminar", bg="#1379ED", command=self.funcionEliminar)
        self.botonEliminar.grid(row=3, column=0, pady=5)  
        
    #Se define la función para el boton "Editar"
    def funcionEditar(self):
        """
        Función de edición para la clase dada.
        Comprueba si la variable `varNombre` no está vacía y llama al método `actualizar` de la clase `Libraries` para actualizar la tabla "Library" en la base de datos con el campo `libraryName` igual al valor de `varNombre` y el campo `libraryID` igual al valor de `varID`.
        Luego, comprueba si la variable `varCiudad` no está vacía y llama al método `actualizar` de la clase `Libraries` para actualizar la tabla "Library" en la base de datos con el campo `city` igual al valor de `varCiudad` y el campo `libraryID` igual al valor de `varID`.
        Por último, llama al método `vincularBaseDatos`.
        """
        if self.varNombre.get():
            self.Libraries.actualizar("Librerias","libraryName",self.varNombre.get(),self.varID.get())   
            
        if self.varCiudad.get():
            self.Libraries.actualizar("Librerias","city",self.varCiudad.get(),self.varID.get())  
            
        self.vincularBaseDatos()
    
    #Se define laSetBranchion para el boton "Eliminar"
    def funcionEliminar(self):
        """
        Elimina los datos de la tabla "Library" basado en el valor almacenado en la variable `varID`.
        
        Parámetros:
            Ninguno
        
        Retorna:
            Ninguno
        """
        self.Libraries.eliminar("Librerias",self.varID.get())
        messagebox.showinfo("DB Librerias","Se eliminaron los datos en la tabla.")
        self.vincularBaseDatos()
        
aplicacion=interfazGrLibrerias()
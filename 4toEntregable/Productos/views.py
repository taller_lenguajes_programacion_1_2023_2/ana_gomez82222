from django.shortcuts import render
from django.db import connection
from .models import producto

# Create your views here.
def categorias(request):
    return render(request, 'categorias.html')
def nuevoProducto(request):
    mensaje=""
    if request.method == 'POST':
        nombre = request.POST['nombre']
        descripcion = request.POST['descripcion']
        categoria = request.POST['categoria']
        cantidad = request.POST['cantidad']
        precio = request.POST['precio']
        imagen = request.POST['imagen']
        imagend1 = request.POST['imagend1']
        imagend2 = request.POST['imagend2']
        nuevo = producto.objects.create(nombre=nombre, descripcion=descripcion, categoria=categoria, cantidad=cantidad, precio=precio, imagen=imagen, imagend1=imagend1, imagend2=imagend2 )
        mensaje="El producto se agrego correctamente"
    return render(request, 'admin_pr.html')
def productos_categoria(request, categoria):
    listaProductos=producto.objects.filter(categoria=categoria).all()
    categ=categoria
    print((categoria))
    return render(request, 'productos.html', { "productos": listaProductos, "categoria": categ})
def verProducto(request, id):
    productos=producto.objects.filter(id=id).all()
    idProducto=id
    print((id))
    return render(request, 'producto.html', { "producto": productos,"id": idProducto})
from django.db import models

# Create your models here.
class producto(models.Model):
    id=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=50)
    descripcion=models.CharField(max_length=150)
    categoria=models.CharField(max_length=50)
    cantidad=models.IntegerField()
    precio=models.IntegerField()
    imagen=models.CharField(max_length=150)
    imagend1=models.CharField(max_length=150)
    imagend2=models.CharField(max_length=150)

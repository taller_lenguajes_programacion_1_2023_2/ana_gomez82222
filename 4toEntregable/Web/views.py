from django.shortcuts import render ,redirect
from .models import cliente

# Create your views here.
def index(request):
    return render(request, 'index.html')

def login(request):
    error=""
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        
        try:
            userEncontrado = cliente.objects.get(username=username)
            if userEncontrado.password == password:
                return redirect('/categorias/')
            else:
                error = "Contraseña incorrecta"
        except:
            error="Usuario no encontrado"
            
    return render(request, 'login.html',{
        "error" : error,
    })
def register(request):
    error=""
    if request.method == 'POST':
        nombre = request.POST['nombre']
        apellido = request.POST['apellido']
        telefono = request.POST['telefono']
        email = request.POST['email']
        username = request.POST['username']
        password = request.POST['password']
        
        try:
            usuarioExiste = cliente.objects.get(username=username)
            error="El usuario ya existe"
        except:
            nuevoCliente = cliente.objects.create(nombre=nombre, apellido=apellido, telefono=telefono, email=email, username=username, password=password)
            return redirect('/login/')
    return render(request, 'register.html')
